#include <iostream>
#include "device/serial/serialclass.h"

using namespace std;

int main()
{
    cout << "Hello, I'm XBEE-TX!" << endl;

    //! Open the serial port that corresponds to your Xbee
    //! Examples:
    //! Linux   : /dev/ttyUSB0
    //! Mac     : /dev/tty.usbserial-A6004byf
    //! Windows : COM1
    //!

    string SerialPort = "/dev/ttyUSB3";
    //    string SerialPort = "/dev/tty.usbserial-DN05L19M";
    size_t SerialBaud = 9600;

    // open serial port
    Serial xbee_tx = Serial(SerialPort,SerialBaud);
    if (xbee_tx.get_error() < 0) {
        cout << "There was a problem opening the serial port. Error # " << xbee_tx.get_error() << endl;
        return 0;
    }

    xbee_tx.set_flowcontrol(1,0);

    //    uint8_t testByte = 'A';
    //    xbee_tx.SendByte(testByte);
    //    xbee_tx.put_char(testByte);

    // send a burst
    string data = "Hello from XBEE-TX";
    cout << "data to send: " << data << endl;
    xbee_tx.put_string(data);

    int count = 0;
    while (1) {
        count++;
        string data = to_string(count);
        data.append(",");
        cout << "data to send: " << data << endl;
        xbee_tx.put_string(data);
        COSMOS_SLEEP(0.5);
    }


    cout << xbee_tx.get_error() << endl;

    cout << "This never ends!" << endl;

    return 0;
}
