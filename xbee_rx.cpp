#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

#include "device/serial/serialclass.h"

using namespace std;

int main()
{

    cout << "Hello Xbee RX!" << endl;

    //! Open the serial port that corresponds to your Xbee
    //! Examples:
    //! Linux   : /dev/ttyUSB0
    //! Mac     : /dev/tty.usbserial-A6004byf
    //! Windows : COM1

    string SerialPort = "/dev/ttyUSB2";
    //    string SerialPort = "/dev/tty.usbserial-DN05L1A5";
    size_t SerialBaud = 9600;

    // open serial port
    Serial xbee_rx = Serial(SerialPort,SerialBaud);

    if (xbee_rx.get_error() < 0) {
        cout << "There was a problem opening the serial port. Error # " << xbee_rx.get_error() << endl;
        return 0;
    }

    //xbee.set_flowcontrol(1,0);

    while(1) {
        // get some characters from xbee-tx
        char buffer = xbee_rx.get_char();

        // only print valid characters
        if (buffer > 0) {
            // print the buffer
            cout << buffer << std::flush;
        }
        // sleep for 0.1 sec
        COSMOS_SLEEP(0.01);
    }

    return 0;
}
