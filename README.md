# README #

This Project is to learn how to connect two XBEE using the COSMOS serial port

Xbees configuration example (configure with XCTU). Using the new XBEE3 (https://www.sparkfun.com/products/15126).

XBEE1
CE: Form Network (coordinator)
ID: 1111
DH: 13A200 (check XBEE2 address high)
DL: ABCDEFGH (check XBEE2 address low)
NI: XBEE1

XBEE2
CE: Join Network 
ID: 1111
DH: 13A200 (check XBEE1 address high)
DL: ABCDEFGH (check XBEE1 address low)
NI: XBEE2


